class VouchersController < ApplicationController
  before_action :authenticate_admin!
  before_action :set_voucher, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource

  # GET /vouchers
  # GET /vouchers.json
  def index
    @vouchers = Voucher.where(used: nil).where(printed: nil)
  end

  # GET /vouchers/1
  # GET /vouchers/1.json
  def show
  end

  def list

    @vouchers = Voucher.where(used: true).order(updated_at: "DESC").page(params[:page]).per(15)

    @usados = Voucher.where(used: true).group(:email).count

  end

  # GET /vouchers/new
  def new
    @voucher = Voucher.new
  end

  # GET /vouchers/1/edit
  def edit
  end

  def print

    @vouchers = Voucher.all
    @vouchers.update_all(printed: true)

    render :json => 1

  end

  # POST /vouchers
  # POST /vouchers.json
  def create

    @voucher = Voucher.new(voucher_params)

    if @voucher.time == '' || @voucher.qtd == ''

      respond_to do |format|
        format.html { redirect_to new_voucher_path, notice: 'Todos os campos são obrigatórios.' }
      end

    else

      qtds = @voucher.qtd.to_i

      qtds.times do
        @voucher.number = SecureRandom.hex(4)

        date = DateTime.now

        sql = "INSERT INTO vouchers (number, time, qtd, created_at, updated_at) VALUES ('#{@voucher.number}', '#{@voucher.time}', '', '#{date}', '#{date}');"
        results = ActiveRecord::Base.connection.execute(sql)

      end

      respond_to do |format|

        format.html { redirect_to @voucher, notice: 'Vouchers criados com sucesso!' }
          format.json { render :show, status: :created, location: @voucher }
      end

    end
  end

  def destroy

    Voucher.destroy_all

  end

  # PATCH/PUT /vouchers/1
  # PATCH/PUT /vouchers/1.json
  def update
    respond_to do |format|
      if @voucher.update(voucher_params)
        format.html { redirect_to @voucher, notice: 'Voucher was successfully updated.' }
        format.json { render :show, status: :ok, location: @voucher }
      else
        format.html { render :edit }
        format.json { render json: @voucher.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /vouchers/1
  # DELETE /vouchers/1.json
  def destroy
    @voucher.destroy
    respond_to do |format|
      format.html { redirect_to vouchers_url, notice: 'Voucher was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_voucher
      @voucher = Voucher.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def voucher_params
      params.require(:voucher).permit(:number, :time, :qtd, :used, :printed)
    end
end
