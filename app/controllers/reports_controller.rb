class ReportsController < ApplicationController
  before_action :authenticate_admin!
  before_action :set_dates
#  load_and_authorize_resource
  def unique_guests
    if has_date?
      @requests = Request.where(created_at: @start_date..@end_date).page(params[:page])
      @graph = Request.where(created_at: @start_date..@end_date).group_by_day(:created_at, format: "%d/%m/%Y").count
    else
      @requests = Request.all.page(params[:page])
      @graph = Request.all.group_by_month(:created_at, format: "%d/%m/%Y").count
    end

    respond_to do |format|
      format.html
      format.csv { send_data @requests.to_csv }
    end
  end

  def recurring_guests
 
    @usuarios = User.where("connections >= 0").order(connections: :desc).page(params[:page]).per(15)
    respond_to do |format|
      format.html
      format.csv { send_data @usuarios.to_csv }
    end
  end

  def registered_users
    @users = User.all.order(id: :desc)
    respond_to do |format|
      format.html
      format.xlsx
    end
  end

  def times_with_more_hits
    if has_date?
      @requests = Request.where(created_at: @start_date..@end_date).group_by_hour(:created_at, format: "%Hh").count
    else
      @requests = Request.all.group_by_hour(:created_at, format: "%Hh").count
    end
    respond_to do |format|
      format.html
      format.csv { send_data Request.generate_csv(@requests, ['Time', 'Quantity']) }
    end
  end

  def operating_sistems
    if has_date?
      @requests = Request.where(created_at: @start_date..@end_date).group(:platform).count
    else
      @requests = Request.all.group(:platform).count
    end

    @devices = count_sistems @requests

    respond_to do |format|
      format.html
      format.csv { send_data Request.generate_csv(@requests, ['Platform', 'Quantity']) }
    end
  end

  def devices
    if has_date_and_campaign?
      @requests = Request.where(created_at: @start_date..@end_date).where(campaign_id: params[:campaign]).group(:platform).count
      @requests_location = Request.where(created_at: @start_date..@end_date).where(campaign_id: params[:campaign]).group(:location).count
      @requests_total = Request.where(created_at: @start_date..@end_date).where(campaign_id: params[:campaign]).count
      @request_male = Request.where(created_at: @start_date..@end_date).where(campaign_id: params[:campaign]).where(gender: "Masculino").count
      @requests_female = Request.where(created_at: @start_date..@end_date).where(campaign_id: params[:campaign]).where(gender: "Feminino").count
      @requests_not = Request.where(created_at: @start_date..@end_date).where(campaign_id: params[:campaign]).where(gender: "").count
      @type_user = Request.where(created_at: @start_date..@end_date).where(campaign_id: params[:campaign]).group(:gender).count
    elsif has_date?
      @requests = Request.where(created_at: @start_date..@end_date).group(:platform).count
      @requests_location = Request.where(created_at: @start_date..@end_date).group(:location).count
      @requests_total = Request.where(created_at: @start_date..@end_date).count
      @requests_male = Request.where(created_at: @start_date..@end_date).where(gender: "Masculino").count
      @requests_female = Request.where(created_at: @start_date..@end_date).where(gender: "Feminino").count
      @requests_not = Request.where(created_at: @start_date..@end_date).where(gender: "").count
      @type_user = Request.where(created_at: @start_date..@end_date).group(:gender).count
    elsif has_campaign?
      @requests = Request.where(campaign_id: params[:campaign]).group(:platform).count
      @requests_location = Request.where(campaign_id: params[:campaign]).group(:location).count
      @requests_total = Request.where(campaign_id: params[:campaign]).count
      @request_male = Request.where(campaign_id: params[:campaign]).where(gender: "Masculino").count
      @requests_female = Request.where(campaign_id: params[:campaign]).where(gender: "Feminino").count
      @requests_not = Request.where(campaign_id: params[:campaign]).where(gender: "").count
      @type_user = Request.where(campaign_id: params[:campaign]).group(:gender).count
    else
      @requests = Request.all.group(:platform).count
      @requests_location = Request.all.group(:location).count
      @requests_total = Request.all.count
      @requests_male = Request.all.where(gender: "Masculino").count
      @requests_female = Request.all.where(gender: "Feminino").count
      @requests_not = Request.all.where(gender: "").count
      @type_user = Request.all.group(:gender).count
    end

    if current_admin.administrador?
      @campaign = Campaign.all
    else
      @campaign = Campaign.where(admin_id: current_admin.id)
    end


    @array = @type_user.map do |name, data|
      if name == nil
        {name: "Não informado", data: data}
      elsif name == ""
        {name: "Fechou sem avisar", data: data}
      else
        {name: name, data: data}
      end
    end

    @devices = count_sistems(@requests)


    @requestsCSV = Request.where(created_at: @start_date..@end_date).where(campaign_id: params[:campaign])

    respond_to do |format|
      format.html
      format.csv { send_data @requestsCSV.to_csv }
    end
  end

  private

  def has_date?
    params[:start_date].present? and params[:campaign].blank?
  end

  def has_date_and_campaign?
    params[:start_date].present? and params[:campaign].present?
  end

  def has_campaign?
    params[:campaign].present? and params[:start_date].blank?
  end


  def set_dates
    @start_date = Date.parse(params[:start_date]) if params[:start_date].present?
    @end_date = Date.parse(params[:end_date]) if params[:start_date].present?
  end

  def count_sistems(request)
    hash = Hash.new
    hash[:android] = 0
    hash[:ios] = 0
    hash[:notebooks] = 0
    hash[:outros] = 0

    request.each do |device, users|
      if device.index(/android/i).present?
        hash[:android] += users
      elsif device.index(/ios/i).present?
        hash[:ios] += users
      elsif device.index(/macintosh/i).present? || device.index(/linux/i).present? || device.index(/windows/i).present? || device.index(/chrome/i).present?
        hash[:notebooks] += users
      else
        hash[:outros] += users
      end
    end

    hash
  end

end
