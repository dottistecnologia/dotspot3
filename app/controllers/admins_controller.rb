class AdminsController < ApplicationController
  before_action :authenticate_admin!
  before_action :set_admin, only: [:show, :edit, :update, :destroy, :credit_history]

  load_and_authorize_resource
  # GET /logers
  # GET /logers.json
  def index
    @admins = Admin.administrador
  end

  def clients
    @admins = Admin.cliente
  end


  # def checkname
  #   if loger.where('logername = ?', params[:loger]).count == 0
  #     render :nothing => true, :status => 200
  #   else
  #     render :nothing => true, :status => 409
  #   end
  #   return
  # end

  # GET /logers/1
  # GET /logers/1.json
  def show
  end

  def credit_history
  end

  # GET /logers/new
  def new
    @admin = Admin.new

  end

  def new_client
    @admin = Admin.new
  end

  # GET /logers/1/edit
  def edit

  end

  # POST /logers
  # POST /logers.json
  def create
    @admin = Admin.new(admin_params)

    if @admin.save
      @admin.cliente? ? r_action = clients_admins_path : r_action = admins_url
      redirect_to r_action
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /logers/1
  # PATCH/PUT /logers/1.json
  def update
    respond_to do |format|

      if @admin.update(admin_params)
        if @admin.cliente?
          format.html { redirect_to clients_admins_path, notice: 'Usuário alterado com sucesso.' }
          format.json { render :show, status: :ok, location: @admin }
        else
          format.html { redirect_to admins_url, notice: 'Usuário alterado com sucesso.' }
          format.json { render :show, status: :ok, location: @admin }

        end
      else
        format.html { render :edit }
        format.json { render json: @admin.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /logers/1
  # DELETE /logers/1.json
  # def destroy
  #   @loger.destroy
  #   respond_to do |format|
  #     format.html { redirect_to logers_url, notice: 'loger was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

   def destroy
     @admin = Admin.find(params[:id])
     @admin.destroy
     respond_to do |format|
       format.html { redirect_to(admins_url) }
       format.js { render :text => "alert('Excluído com sucesso.')" }
     end
   end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin
      @admin = Admin.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_params
      params.require(:admin).permit(:email, :password, :admin, :name, :role, :phone, :cpf_cnpj)
    end
end
