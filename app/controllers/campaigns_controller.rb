class CampaignsController < ApplicationController
  before_action :authenticate_admin!, except: [:view, :pspr, :votes]
  before_action :set_campaign, only: [:show, :edit, :update, :destroy]

  load_and_authorize_resource except: [:view, :votes]

  # GET /campaigns
  # GET /campaigns.json
  def index
    if current_admin.cliente?
      @campaigns = current_admin.campaigns
    else
      @campaigns = Campaign.all
    end
  end

  # GET /campaigns/1
  # GET /campaigns/1.json
  def show

    @campaign = Campaign.find(params[:id])
    respond_to do |format|
      format.html
      format.js
    end

  end

  def view
    require "browser"

    session[:mac] = params[:mac] unless params[:mac].nil?
    session[:ssid] = params[:ssid] unless params[:ssid].nil?
    session[:location] = params[:location] unless params[:location].nil?

    campaigns = Campaign.date_valid.where("credits > 0").joins(:routers).where("routers.location = ?", session[:location])

    if campaigns.any?
      weights = campaigns.map {|c| c.weight} # pega lista dos pesos
      ps = weights.map { |w| (Float w) / weights.reduce(:+) } # normaliza os pesos
      loaded_campaigns = campaigns.zip(ps).to_h # monta hash de campanhas com pesos normalizados
      @campaign = loaded_campaigns.max_by { |_, weight| rand ** (1.0 / weight) }.first # pega uma camapanha

      @answers = Answer.where(quiz_id: @campaign.quiz)

    end

    @router = Router.find_by_location(session[:location])
    @mac = User.where(mac: session[:mac]).last

    if !@mac.nil?
      @tmp = @mac.id
      @gender = @mac.gender
    else
      @tmp = 0
    end

    Request.create(
      mac: params[:mac],
      location: params[:location],
      ssid: params[:ssid],
      platform: "#{browser.platform.name} #{browser.platform.version}",
      device: browser.device.name,
      browser: "#{browser.name} #{browser.version}",
      user_id: @tmp,
      gender: @gender,
      campaign: @campaign)

    if @campaign.present?
      @campaign.views += 1
      @campaign.save
      @campaign.decrease_credit
    end

  end


  def votes

    @answer = Answer.find(params[:id])
    @answer.votes += 1
    @answer.save
    router = Router.find_by_location(session[:location])

    @vote = Vote.new
    @vote.mac = session[:mac]
    @vote.answer_id = @answer.id
    @vote.quiz_id = @answer.quiz.id
    @vote.save

    redirect_to guest_root_url

  end


  # GET /campaigns/new
  def new
    @campaign = Campaign.new
    
  end

  # GET /campaigns/1/edit
  def edit
  end

  # POST /campaigns
  # POST /campaigns.json
  def create
    @campaign = Campaign.new(campaign_params)

    @campaign.admin ||= current_admin

    if current_admin.cliente?
      @campaign.status = false
    else
      @campaign.status = true
    end

    respond_to do |format|
      if @campaign.save
        format.html { redirect_to @campaign, notice: 'Campanha criada com sucesso.' }
        format.json { render :show, status: :created, location: @campaign }
      else
        format.html { render :new }
        format.json { render json: @campaign.errors, status: :unprocessable_entity }
      end
    end
  end

  def disable
    @campaign = Campaign.update(params[:id], status: nil, credits: 0)

    respond_to do |format|
      format.html { redirect_to campaigns_url, notice: 'Campanha desativada com sucesso.' }
      format.json { head :no_content }
    end
  end

  def enable
    if current_admin.cliente?
      @campaign = Campaign.update(params[:id], status: false)
    else
      @campaign = Campaign.update(params[:id], status: true)
    end

    respond_to do |format|
      format.html { redirect_to campaigns_url, notice: 'Campanha ativada com sucesso.' }
      format.json { head :no_content }
      format.js
      
    end
  end
  # PATCH/PUT /campaigns/1
  # PATCH/PUT /campaigns/1.json
  def update

    if current_admin.cliente?
      @campaign.status = false
    end

    respond_to do |format|
      if @campaign.update(campaign_params)
        format.html { redirect_to campaigns_url, notice: 'Campanha atualizada com sucesso.' }
        format.json { render :show, status: :ok, location: @campaign }
      else
        format.html { render :edit }
        format.json { render json: @campaign.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /campaigns/1
  # DELETE /campaigns/1.json
  def destroy
    @campaign.destroy
    respond_to do |format|
      format.html { redirect_to campaigns_url, notice: 'Campanha excluída com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_campaign
      @campaign = Campaign.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def campaign_params
      params.require(:campaign).permit(:name, :stard_date, :end_date, :video, :default, :imagem, :quiz_id, :weight, :credits, :admin_id, :status, :router_ids => [])
    end
end
