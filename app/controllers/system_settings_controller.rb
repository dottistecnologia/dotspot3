class SystemSettingsController < ApplicationController
  before_action :authenticate_admin!
  before_action :set_system_setting, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource
  # GET /system_settings
  # GET /system_settings.json


  def my
    @system_setting = SystemSetting.first
  end

  # PATCH/PUT /system_settings/1
  # PATCH/PUT /system_settings/1.json
  def update
    respond_to do |format|
      if @system_setting.update(system_setting_params)
        format.html { redirect_to my_system_settings_path @system_setting, notice: 'System setting was successfully updated.' }
        format.json { render :show, status: :ok, location: @system_setting }
      else
        format.html { render :my }
        format.json { render json: @system_setting.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_system_setting
      @system_setting = SystemSetting.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def system_setting_params
      params[:system_setting].delete(:password) if params[:system_setting][:password].blank?
      params.require(:system_setting)
        .permit(
          :allow_guest_self_sign_up,
          :terms_and_conditions,
          :allow_facebook_signup,
          :facebook_checking,
          :place_id_to_facebook_checkin,
          :logo,
          :background,
          :gender_visible,
          :gender_required,
          :name_visible,
          :name_required,
          :cpf_visible,
          :cpf_required,
          :birthday_visible,
          :birthday_required,
          :email_visible,
          :email_required,
          :telephone_visible,
          :telephone_required,
          :address_visible,
          :address_required,
          :sunday_opens_at,
          :sunday_closes_at,
          :monday_opens_at,
          :monday_closes_at,
          :tuesday_opens_at,
          :tuesday_closes_at,
          :wednesday_opens_at,
          :wednesday_closes_at,
          :thursday_opens_at,
          :thursday_closes_at,
          :friday_opens_at,
          :friday_closes_at,
          :saturday_opens_at,
          :saturday_closes_at,
          :site,
          :host,
          :port,
          :user,
          :password
        )
    end
end
