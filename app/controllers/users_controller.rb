class UsersController < ApplicationController
  before_action :authenticate_admin!, except: [:guest_cadastro, :create_guest, :forgot_password, :change_password]
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  before_action :load_settings, only: [:guest_cadastro, :create_guest]

  # GET /users
  # GET /users.json
  def index

    @filterrific = initialize_filterrific(
        User,
        params[:filterrific],
        default_filter_params: {},
        sanitize_params: true,
    ) || return
    @users = @filterrific.find.page(params[:page]).order(created_at: :desc)

    respond_to do |format|
      format.html
      format.js
    end

  end


  def verify_whatsapp

    @user = User.find(params[:token])

    respond_to do |format|
      if @user.update(whatsapp_verified: true)
        format.html { render :text => "OK" }
        # format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        # format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def verify_email

    @user = User.find(params[:token])

    respond_to do |format|
      if @user.update(email_verified: true)
        format.html { render :text => "OK" }
        # format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        # format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def active
    @usuarios = Array.new

    # Router.all.each do |router|
    router = Router.first

      ip = router.ip_out
      port = router.port_out
      username = router.username
      password = router.password
      check = Net::Ping::External.new(router.ip_out)
      puts "ROUTER #{router.ip_out}".on_yellow
      if check.ping?
        puts "PINGOU ROUTER #{router.ip_out}".on_yellow
         begin
        tik = MTik::Connection.new(:host => ip, :user => username, :pass => password, :port => port)
         rescue MTik::Error, Errno::ETIMEDOUT, Errno::EHOSTUNREACH, Errno::ECONNREFUSED => e
           print "ERROR CONNECTING OR LOGGING IN: #{e}\n"
          #  next
         end

        @users = {}
        tik.get_reply_each('/ip/hotspot/active/getall') do |r, s|
          if s.key?('!re') && s.key?('.id')
            @users[s['.id']] = {
                :address => s['address'],
                :user => s['user'],
                :comment => s['comment'],
                :server => s['server'],
                :uptime => s['uptime'],
                :macaddress => s['mac-address'],
                :disabled => s['disabled'] == true,
                :id => s['.id']
            }
          end
        end

        tik.close
        @users.each { |hotspot_user| @usuarios << hotspot_user[1] }

        puts "FEITA REQUISIÂO ROUTER #{router.location}".on_yellow
      end

    # end

    respond_to do |format|
      format.html { render :active }
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show

    @user = User.find(params[:id])

    array = Request.where(user_id: @user.id).group(:location).count

    @series = [{name: "Recorrência", data: array}]

    @graph = array.map do |n, i|
      {
          name: n,
          data: i
      }
    end
    @requests_total = Request.where(user_id: @user.id).count

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /users/new
  def new
    @user = User.new

    notice = nil

    router = Router.first

    ip = router.ip_out
    port = router.port_out
    username = router.username
    password = router.password

    begin
      tik = MTik::Connection.new(:host => ip, :user => username, :pass => password, :port => port)
    rescue MTik::Error, Errno::ETIMEDOUT, Errno::EHOSTUNREACH, Errno::ECONNREFUSED => e
      print "ERROR CONNECTING OR LOGGING IN: #{e}\n"
      exit
    end

    @profiles = {}
    tik.get_reply_each('/ip/hotspot/user/profile/getall') do |r, s|


      if s.key?('!re') && s.key?('.id')
        @profiles[s['.id']] = {
            :name => s['name'],
            :sessiontimeout => s['session-timeout'],
            :sharedusers => s['shared-users'],
            :ratelimit => s['rate-limit'],
            :id => s['.id']
        }
      end

    end

    # puts @profiles

    @perfis = Array.new
    @profiles.each { |profile| @perfis << profile[1] }
    tik.close
  end

  def guest_cadastro

    @user = User.find_by(mac: session[:mac])
    if @user.present?
      if @user.connections.nil?
        @user.connections = 0
        @user.update(connections: @user.connections + 1)
        @user.macs.create(mac: session[:mac]) unless @usuario.macs.find_by(mac: session[:mac])
      else
        @user.update(connections: @user.connections + 1)
        @user.macs.create(mac: session[:mac]) unless @usuario.macs.find_by(mac: session[:mac])
      end
    else
      @user = User.new(
        email: params[:email],
        name: params[:name]
      )

      @user.profile = 'default'
      notice = nil
    end
    
  end

  def create_guest

    router = Router.find_by_location(session[:location])

    ip = router.ip_out
    port = router.port_out
    username = router.username
    password = router.password

    @user = User.find_by(mac: session[:mac])
    @user = User.find_by(name: params[:name]) unless @user.present?

    if @user.present?
      if @user.connections.nil?
        @user.connections = 0
        @user.update(connections: @user.connections + 1)
        @user.macs.create(mac: session[:mac]) unless @usuario.macs.find_by(mac: session[:mac])
      else
        @user.update(connections: @user.connections + 1)
      end
      respond_to do |format|
        format.html { redirect_to "http://#{router.ip}/login?username=admin&password=admin" }
      end
    else
      @user = User.new(user_params)

      @user.mac = session[:mac]

      respond_to do |format|
        if @user.save
          format.html { redirect_to "http://#{router.ip}/login?username=admin&password=admin" }
        else
          format.html { render :guest_cadastro }
        end
        # end
        # tik.close
      end
    end

    

  end


  def forgot_password

  end

  def change_password
    user = User.find_by_cpf(params[:cpf].gsub(/\D/, ''))

    if user.present? and user.date == Date.parse(params[:date]) and params[:new_password].present?
      user.update(password: params[:new_password])
      redirect_to guest_root_path, alert: "Senha alterada com sucesso! Faça o login com sua nova senha."
    else
      redirect_to forgot_password_users_path, alert: "As informações não conferem, verifique e tente novamente!"
    end
  end

  # GET /users/1/edit
  def edit

  end

  def destroy_multiple

    User.destroy(params[:users])

    respond_to do |format|
      format.html { redirect_to users_path }
      format.json { head :no_content }
    end

  end

  # POST /users
  # POST /users.json
  def create

    Router.all.each do |router|

      ip = router.ip_out
      port = router.port_out
      username = router.username
      password = router.password


      @user = User.new(user_params)
      @user.email = @user.email.downcase
      @user.password = @user.password.downcase


      begin
        tik = MTik::Connection.new(:host => ip, :user => username, :pass => password, :port => port)
        puts "COMUNICOU COM O ROUTER #{router.location}".on_yellow
      rescue MTik::Error, Errno::ETIMEDOUT, Errno::EHOSTUNREACH, Errno::ECONNREFUSED => e
        print "ERROR CONNECTING OR LOGGING IN: #{e}\n"
      end
      tik.get_reply(
          "/ip/hotspot/user/add", "=name=#{@user.email}", "=password=#{@user.password}", "=profile=#{@user.profile}"
      )
      tik.close

    end

    respond_to do |format|
      if @user.save
        format.html { redirect_to users_path, notice: 'Usuario criado com sucesso.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { redirect_to new_user_path, notice: 'Erro ao criar usuário, pode ser que o mesmo já exista.' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end

  end

  # APARENTEMENTE SEM USO
  def remove

    router = Router.first

    ip = router.ip_out
    port = router.port_out
    username = router.username
    password = router.password

    begin
      tik = MTik::Connection.new(:host => ip, :user => username, :pass => password, :port => port)
    rescue MTik::Error, Errno::ETIMEDOUT, Errno::EHOSTUNREACH, Errno::ECONNREFUSED => e
      print "ERROR CONNECTING OR LOGGING IN: #{e}\n"
      # exit
    end

    users = {}
    tik.get_reply_each('/ip/hotspot/active/getall') do |r, s|
      if s.key?('!re') && s.key?('mac-address')
        users[s['mac-address']] = {
            :macaddress => s['mac-address'],
            :id => s['.id']
        }
      end
    end


    username = params[:username]
    id = users["#{username}"][:id]
    teste = tik.get_reply('/ip/hotspot/active/remove', "=.id=#{id}") if users.key?("#{username}")

    users = {}
    tik.get_reply_each('/ip/hotspot/cookie/getall') do |r, s|
      if s.key?('!re') && s.key?('mac-address')
        users[s['mac-address']] = {
            :macaddress => s['mac-address'],
            :id => s['.id']
        }
      end
    end

    username = params[:username]
    id = users["#{username}"][:id]
    teste = tik.get_reply('/ip/hotspot/cookie/remove', "=.id=#{id}") if users.key?("#{username}")

    tik.close
    respond_to do |format|
      format.html { redirect_to active_users_path }
    end


  end

  def addmac
    router = Router.find(params[:router_id])

    ip = router.ip_out
    port = router.port_out
    username = router.username
    password = router.password

    begin
      tik = MTik::Connection.new(:host => ip, :user => username, :pass => password, :port => port)
    rescue MTik::Error, Errno::ETIMEDOUT, Errno::EHOSTUNREACH, Errno::ECONNREFUSED, Errno::ENETUNREACH => e
      print "ERROR CONNECTING OR LOGGING IN: #{e}\n"
      respond_to do |format|
        format.html { render :text => "ERROR CONNECTING OR LOGGING IN: #{e}" }
        format.json { render json: 'error' }
      end
      return
    end

    mac = params[:mac]
    comment = params[:comment]
    status = params[:status]

    tik.get_reply(
        "/ip/hotspot/ip-binding/add", "=mac-address=#{mac}", "=comment=#{comment}", "=server=#{router.location.gsub('bahiabonita-', '').capitalize}", "=type=#{status}"
    )

    puts router.location.gsub('bahiabonita-', '').capitalize

    tik.close
    respond_to do |format|
      format.html { render :text => :ok }
      format.json
    end
  end

  def listmac

    @usuarios = Array.new

    # Router.all.each do |router|
    router = Router.first

      # if router.status == true
        ip = router.ip_out
        port = router.port_out
        username = router.username
        password = router.password

        begin
          tik = MTik::Connection.new(:host => ip, :user => username, :pass => password, :port => port)
        rescue MTik::Error, Errno::ETIMEDOUT, Errno::EHOSTUNREACH, Errno::ECONNREFUSED => e
          print "ERROR CONNECTING OR LOGGING IN: #{e}\n"
          # next
        end

        @users = {}
        tik.get_reply_each('/ip/hotspot/ip-binding/getall') do |r, s|


          if s.key?('!re') && s.key?('.id')
            @users[s['.id']] = {
                :server => s['server'],
                :comment => s['comment'],
                :type => s['type'],
                :macaddress => s['mac-address'],
                :disabled => s['disabled'] == true,
                :id => s['.id'],
                :router => router.location
            }
          end

        end

        @users.each do |hotspot_user|
          @usuarios << hotspot_user[1]
        end

        tik.close
      # end
    # end

    respond_to do |format|
      format.html { render :listmac }
    end


  end

  def removemac


    router = Router.find_by_location(params[:router_location])
    ip = router.ip_out
    port = router.port_out
    username = router.username
    password = router.password

    begin
      tik = MTik::Connection.new(:host => ip, :user => username, :pass => password, :port => port)
    rescue MTik::Error, Errno::ETIMEDOUT, Errno::EHOSTUNREACH, Errno::ECONNREFUSED => e
      print "ERROR CONNECTING OR LOGGING IN: #{e}\n"
      exit
    end


    users = {}
    tik.get_reply_each('/ip/hotspot/ip-binding/getall') do |r, s|
      if s.key?('!re') && s.key?('mac-address')
        users[s['mac-address']] = {
            :macaddress => s['mac-address'],
            :id => s['.id']
        }
      end
    end


    mac = params[:mac]

    if users.key?("#{mac}")
      id = users["#{mac}"][:id]
      teste = tik.get_reply('/ip/hotspot/ip-binding/remove', "=.id=#{id}") if users.key?("#{mac}")
    end
    tik.close

    respond_to do |format|
      format.html { redirect_to listmac_users_path }
    end


  end


  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to users_path, notice: 'usuário atualizado com sucesso!' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy

    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'Usuario excluído com sucesso.' }
      format.json { head :no_content }
    end
  end

  def addprofile


    router = Router.find(params[:router_id])

    ip = router.ip_out
    port = router.port_out
    username = router.username
    password = router.password

    begin
      tik = MTik::Connection.new(:host => ip, :user => username, :pass => password, :port => port)
    rescue MTik::Error, Errno::ETIMEDOUT, Errno::EHOSTUNREACH, Errno::ECONNREFUSED, Errno::ENETUNREACH => e
      print "ERROR CONNECTING OR LOGGING IN: #{e}\n"
      respond_to do |format|
        format.html { render :text => "ERROR CONNECTING OR LOGGING IN: #{e}" }
        format.json { render json: 'error' }
      end
      return
    end

    name = params[:name]
    sessiontimeout = params[:sessiontimeout].present? ? params[:sessiontimeout] : '1h'
    sharedusers = params[:sharedusers].present? ? params[:sharedusers] : 'unlimited'
    ratelimit = params[:ratelimit]

    tik.get_reply(
        "/ip/hotspot/user/profile/add", "=name=#{name}", "=session-timeout=#{sessiontimeout}", "=shared-users=#{sharedusers}", "=rate-limit=#{ratelimit}"
    )
    tik.close

    respond_to do |format|
      format.html { render :text => 'ok' }
    end
  end

  def listprofile
    @perfis = Array.new

    Router.all.each do |router|

      # if router.status == true

        ip = router.ip_out
        port = router.port_out
        username = router.username
        password = router.password

        begin
          tik = MTik::Connection.new(:host => ip, :user => username, :pass => password, :port => port)
        rescue MTik::Error, Errno::ETIMEDOUT, Errno::EHOSTUNREACH, Errno::ECONNREFUSED => e
          print "ERROR CONNECTING OR LOGGING IN: #{e}\n"
          next
        end

        @profiles = {}
        tik.get_reply_each('/ip/hotspot/user/profile/getall') do |r, s|

          if s.key?('!re') && s.key?('.id')
            @profiles[s['.id']] = {
                :name => s['name'],
                :sessiontimeout => s['session-timeout'],
                :sharedusers => s['shared-users'],
                :ratelimit => s['rate-limit'],
                :id => s['.id'],
                :router => router.location
            }
          end

        end

        @profiles.each do |profile|
          @perfis << profile[1]
        end

        tik.close
      # end

    end

    respond_to do |format|
      format.html { render :listprofile }
    end


  end


  def removeprofile

    router = Router.find_by_location(params[:router_location])

    ip = router.ip_out
    port = router.port_out
    username = router.username
    password = router.password

    begin
      tik = MTik::Connection.new(:host => ip, :user => username, :pass => password, :port => port)
      puts "COMUNICOU COM ROUTER #{router.location}".on_yellow
    rescue MTik::Error, Errno::ETIMEDOUT, Errno::EHOSTUNREACH, Errno::ECONNREFUSED => e
      print "ERROR CONNECTING OR LOGGING IN: #{e}\n"
      exit
    end


    profiles = {}
    tik.get_reply_each('/ip/hotspot/user/profile/getall') do |r, s|
      if s.key?('!re') && s.key?('name')
        profiles[s['name']] = {
            :name => s['name'],
            :id => s['.id']
        }
      end
    end


    name = params[:name]


    if profiles.key?("#{name}")
      id = profiles["#{name}"][:id]
      teste = tik.get_reply('/ip/hotspot/user/profile/remove', "=.id=#{id}") if profiles.key?("#{name}")
    end

    tik.close

    respond_to do |format|
      format.html { redirect_to listprofile_users_path }
    end


  end

  private

  def load_settings
    @system_setting = SystemSetting.first
  end

  def set_user
    @user = User.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:name, :gender, :profile, :category, :address, :number, :city, :state, :district, :code, :active, :registry, :issuing, :cpf, :date, :validity, :mac, :ssid, :ap, :registration, :company, :depend, :dependent, :email, :telephone, :voucher, :password)
  end
end
