class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  helper_method :current_guest, :system_settings
  

  def current_guest
    return unless session[:guest_id]
    @current_guest ||= User.find(session[:guest_id])
  end

  def system_settings
    @system_settings ||= SystemSetting.first
  end

  # Sobrescreve a config do cancancan pra user 'current_admin' ao invés de 'current_user'
  def current_ability
    @current_ability ||= Ability.new(current_admin)
  end

end
