class HomeController < ApplicationController
  before_action :authenticate_admin!


  def index

    redirect_to reports_devices_path if current_admin.cliente?

    @mes = Voucher.all
    @campanhas_views = Campaign.all.sum(:views)
    @locations = Router.all

    if params[:location].nil? || params[:location] == ""
      @users_today = User.where(created_at: Date.today.beginning_of_day..Date.today.end_of_day).count
      @users_count = User.all.count
      @campanhas_views = Request.all.count
      @campanhas_views_today = Request.where(created_at: Date.today.beginning_of_day..Date.today.end_of_day).count
      @recorrentes = User.where("connections >= 0").order(connections: :desc).page(params[:page]).per(10)
      @requests_group = Request.all.group(:user_id).order('count_user_id DESC').count('user_id')

    else
      @users_today = User.where(location: params[:location]).where(created_at: Date.today.beginning_of_day..Date.today.end_of_day).count
      @users_count = User.where(location: params[:location]).count
      @campanhas_views = Request.where(location: params[:location]).count
      @campanhas_views_today = Request.where(location: params[:location]).where(created_at: Date.today.beginning_of_day..Date.today.end_of_day).count
      @recorrentes = User.where("connections >= 0").where(location: params[:location]).order(connections: :desc).page(params[:page]).per(10)
      @requests_group = Request.where(location: params[:location]).group(:user_id).order('count_user_id DESC').count('user_id')

    end

  end

  def list_aps
    unifi = SystemSetting.first

    # begin
      unifi = Unifi::Api::Controller.new("#{unifi.host}", "#{unifi.user}",  "#{unifi.password}", 8443, 'v4', "#{unifi.site}")
      @devices = unifi.get_aps

      render partial: "list_aps"
  end

  def top_users

    @recorrentes = User.where("connections >= 0").order(connections: :desc).page(params[:page]).per(10)
    render partial: "top_users"

  end


  def clients
    unifi = SystemSetting.first
    begin
        unifi = Unifi::Api::Controller.new("#{unifi.host}", "#{unifi.user}",  "#{unifi.password}", 8443, 'v4', "#{unifi.site}")
      
        devices = unifi.get_aps
        @device = devices.select { |device| device['mac'] == params[:apmac] }
        clients = unifi.get_clients
        @clients = clients.select { |client| client['ap_mac'] == params[:apmac] }

    rescue StandardError => e
      flash[:alert] =  e
      @devices = []
      @total = 0
    end

    respond_to do |format|
      format.html
      format.js
    end

  end

end
