class ChartsController < ApplicationController

  def users_per_day
    render json: User.group_by_day_of_week(:created_at, format: "%a").count
  end

  def vouchers_use
  	render json: Voucher.where(used: true).group(:email).count
  end

  def last_campaign


  	render json: Request.group(:browser).count

  	
  end

end