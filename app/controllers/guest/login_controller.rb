class Guest::LoginController < ApplicationController
  skip_before_filter :verify_authenticity_token
  before_action :load_settings, only: [:novo, :caldas]

  require 'httparty'
  require 'ostruct'
  require 'json'
  require 'nokogiri'
  require 'open-uri'

  layout 'guest'

  def set_access_control_headers
    headers['Access-Control-Allow-Origin'] = "*"
    headers['Access-Control-Request-Method'] = %w{GET POST OPTIONS}.join(",")
  end

  def index
  end

  def indexgiraffas
  end

  def indexhospedes
  end

  def caldas
  end

  def novo
    session[:mac] = params[:mac] unless params[:mac].nil?
    session[:ssid] = params[:ssid] unless params[:ssid].nil?
    session[:location] = params[:location] unless params[:location].nil?
    @router = Router.find_by_location(session[:location])
    @mac = User.where(mac: params[:mac]).last
    @campaign = Campaign.date_valid.offset(rand(Campaign.date_valid.count)).first

    if @mac.present?
      router = Router.find_by_location(session[:location])
      redirect_to "http://#{router.ip}/login?username=admin&password=admin"
    else
      redirect_to guest_cadastro_users_path
    end
  end

  def pspr
    session[:mac] = params[:mac] unless params[:mac].nil?
    session[:ssid] = params[:ssid] unless params[:ssid].nil?
    session[:location] = params[:location] unless params[:location].nil?
    @router = Router.find_by_location(session[:location])
    @mac = Mac.where(mac: session[:mac]).last
    @system_setting = SystemSetting.first
    @campaign = Campaign.date_valid.offset(rand(Campaign.date_valid.count)).first
  end

  def novo_login
    email = params[:email]
    password = params[:password]

    if User.where(:email => email).present?

      @usuario = User.find_by(:email => email)

      if @usuario.password == password
        session[:guest_id] = @usuario.id

        if @usuario.connections.nil?
          @usuario.connections = 0
          @usuario.update(connections: @usuario.connections + 1)
          @usuario.macs.create(mac: session[:mac]) unless @usuario.macs.find_by(mac: session[:mac])
        else
          @usuario.update(connections: @usuario.connections + 1)
        end

        mac = session[:mac]
        comment = @usuario.email

        router = Router.find_by_location(session[:location])


        @usuario.update(last_login: Time.now)
        respond_to do |format|
          format.html { render :text => @usuario.email }
        end


      else
        respond_to do |format|
          format.html { render :text => "Email e/ou senha incorretos." }
        end
      end
    else
      respond_to do |format|
        format.html { render :text => "Este email não foi encontrado em nossa base de dados." }
      end
    end
  end

  def login_facebook
    router = Router.find_by_location(session[:location])
    ip = router.ip_out
    port = router.port_out
    username = router.username
    password = router.password

    if User.find_by_email(env['omniauth.auth'].info.email).present?
      user = User.from_omniauth(env['omniauth.auth'], session[:location])
      user.update(mac: session[:mac], connections: user.connections + 1)
      mac = session[:mac]
      comment = user.email
    else
      user = User.new
      user.name = env['omniauth.auth'].info.name
      user.email = env['omniauth.auth'].info.email

      if user.save
        mac = session[:mac]
        comment = user.email
			else
				redirect_to "http://#{router.ip}/login?username=admin&password=admin"
      end
    end
  end

  private

  def load_settings
    @system_setting = SystemSetting.first
  end

end
