class TechnicalCallsController < ApplicationController
  before_action :authenticate_admin!, except: [:open, :create]
  before_action :set_technical_call, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource
  # GET /technical_calls
  # GET /technical_calls.json
  def index
    @technical_calls = TechnicalCall.all
  end

  # GET /technical_calls/1
  # GET /technical_calls/1.json
  def show
  end

  # GET /technical_calls/new
  def new
    @technical_call = TechnicalCall.new
  end

  def open
    @technical_call = TechnicalCall.new
    @technical_call.user = User.find_by_mac(session[:mac])
  end

  # GET /technical_calls/1/edit
  def edit
  end

  # POST /technical_calls
  # POST /technical_calls.json
  def create
    @technical_call = TechnicalCall.new(technical_call_params)
    @technical_call.status = 'aberto'

    respond_to do |format|
      if @technical_call.save
        format.html { redirect_to technical_calls_path, notice: 'Chamado aberto com sucesso.' }
        format.json { render :show, status: :created, location: @technical_call }
      else
        format.html { render :new }
        format.json { render json: @technical_call.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /technical_calls/1
  # PATCH/PUT /technical_calls/1.json
  def update
    respond_to do |format|
      if @technical_call.update(technical_call_params)
        format.html { redirect_to technical_calls_path, notice: 'Chamado atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @technical_call }
      else
        format.html { render :edit }
        format.json { render json: @technical_call.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /technical_calls/1
  # DELETE /technical_calls/1.json
  def destroy
    @technical_call.destroy
    respond_to do |format|
      format.html { redirect_to technical_calls_url, notice: 'Chamado excluído com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_technical_call
      @technical_call = TechnicalCall.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def technical_call_params
      params.require(:technical_call).permit(:user_id, :description, :status)
    end
end
