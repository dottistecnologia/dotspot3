class Request < ActiveRecord::Base
  belongs_to :campaign
  belongs_to :user


  def self.to_csv(options = {})
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |obj|
        csv << obj.attributes.values_at(*column_names)
      end
    end
  end

  def self.generate_csv(objs,titles,options = {})
    CSV.generate(options) do |csv|
      csv << titles
      objs.each do |obj|
        csv << obj
      end
    end
  end

end
