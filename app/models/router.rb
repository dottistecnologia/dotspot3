class Router < ActiveRecord::Base
  has_and_belongs_to_many :campaigns

  validates :location, uniqueness: true

  def name
    location
  end

end
