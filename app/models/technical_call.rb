class TechnicalCall < ActiveRecord::Base
  belongs_to :user
  enum status:{ fechado: 0, aberto: 1, cancelado: 2 }
end
