class Admin < ActiveRecord::Base
  has_many :credits, :dependent => :delete_all
  has_many :campaigns, :dependent => :delete_all
  has_many :quizzes, :dependent => :delete_all
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :trackable, :validatable

  enum role: { administrador: 0, cliente: 1}
  
end
