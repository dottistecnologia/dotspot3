class Campaign < ActiveRecord::Base
    mount_uploader :imagem, CampaignImageUploader
    has_and_belongs_to_many :routers
    has_many :requests
    belongs_to :quiz
    belongs_to :admin

    after_create :decrease_credits_from_client_on_create
    after_update :decrease_credits_from_client_on_update

    scope :date_valid, lambda {where("stard_date <= ? AND end_date >= ?", Date.today, Date.today )}

    validates_presence_of :weight, :name, :stard_date, :end_date, :admin_id
    validates :credits, numericality: { greater_than_or_equal_to: 0 }

    validate :credits_availability

    def credits_difference
      if self.credits_was.nil?
        self.credits
      else
        self.credits - self.credits_was
      end
    end

    # Usado ao mostra a campanha
    def decrease_credit
      self.update_column(:credits, self.credits - 1)
    end

    private
      def decrease_credits_from_client_on_create
        self.admin.credit -= self.credits
        self.admin.save
      end

      def decrease_credits_from_client_on_update
        self.admin.credit -= self.credits_difference
        self.admin.save
      end

      def credits_availability
        self.errors[:credits] << "Créditos insuficiêntes. Saldo disponível: #{self.admin.credit}." unless self.credits_difference <= self.admin.credit
      end
end
