class Voucher < ActiveRecord::Base
	# validates :time, presence: true
	validates :number, length: 1..10, allow_blank: false
end
