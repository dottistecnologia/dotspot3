class User < ActiveRecord::Base
  has_many :macs, dependent: :destroy
  has_many :technical_calls, dependent: :destroy
  has_many :requests

	validates :email, :uniqueness => {:allow_blank => true}
  # validates_presence_of :cpf


  # Setter do cpf. Remove as pontuação e guarda somente os números
  def cpf=(val)
    write_attribute(:cpf, val.gsub(/\D/, ''))
  end


	scope :sorted_by, lambda { |sort_option|
	  # extract the sort direction from the param value.
	  direction = (sort_option =~ /desc$/) ? 'desc' : 'asc'
	  case sort_option.to_s
	  when /^created_at_/
	    # Simple sort on the created_at column.
	    # Make sure to include the table name to avoid ambiguous column names.
	    # Joining on other tables is quite common in Filterrific, and almost
	    # every ActiveRecord table has a 'created_at' column.
	    order("users.created_at #{ direction }")

	  else
	    raise(ArgumentError, "Invalid sort option: #{ sort_option.inspect }")
	  end
	}

  scope :search_query, lambda { |query|
    return nil  if query.blank?
    # condition query, parse into individual keywords
    query = query.to_s
    terms = query.downcase.split(/\s+/)
    # replace "*" with "%" for wildcard searches,
    # append '%', remove duplicate '%'s
    terms = terms.map { |e|
      (e.gsub('*', '%') + '%').gsub(/%+/, '%')
    }
    # configure number of OR conditions for provision
    # of interpolation arguments. Adjust this if you
    # change the number of OR conditions.
    num_or_conditions = 1
    where(
      terms.map {
        or_clauses = [
          "LOWER(users.name) LIKE ?"
        ].join(' OR ')
        "(#{ or_clauses })"
      }.join(' AND '),
      *terms.map { |e| [e] * num_or_conditions }.flatten
    )
  }

	filterrific(
	  default_filter_params: { sorted_by: 'created_at_desc' },
	  available_filters: [
	    :sorted_by,
	    :search_query,
	    :email
	  ]
	)


  validates_presence_of :gender, if: Proc.new {|p| SystemSetting.first.gender_required? }
  validates_presence_of :name, if: Proc.new {|p| SystemSetting.first.name_required? }
  validates_presence_of :cpf, if: Proc.new {|p| SystemSetting.first.cpf_required? }
  validates_presence_of :date, if: Proc.new {|p| SystemSetting.first.birthday_required? }
  validates_presence_of :email, if: Proc.new {|p| SystemSetting.first.email_required? }
  validates_presence_of :address, :number, :city, :state, :district, if: Proc.new {|p| SystemSetting.first.address_required? }

  def self.to_csv(options = {})
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |obj|
        csv << obj.attributes.values_at(*column_names)
      end
    end
  end

  def self.from_omniauth(auth, location)
    # if not User.where(provider: auth.provider, uid: auth.uid).any?
    if not User.where(email: auth.info.email).any?
      router = Router.find_by_location(location)

      ip = router.ip
      username = router.username
      password = router.password

      begin
        tik = MTik::Connection.new(:host => ip, :user => username, :pass => password)
      rescue MTik::Error,  Errno::ETIMEDOUT, Errno::EHOSTUNREACH, Errno::ECONNREFUSED => e
        print "ERROR CONNECTING OR LOGGING IN: #{e}\n"
      end
      tik.get_reply(
        "/ip/hotspot/user/add", "=name=#{auth.info.email}", "=password=fraiburgo123", "=profile=#{'default'}"

      ) do |request, sentence|
        puts "Comunicou com Mikrotik"
      end
      tik.close
    end

    # where(provider: auth.provider, uid: auth.uid).first_or_initialize.tap do |user|
    where(email: auth.info.email).first_or_initialize.tap do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.name = auth.info.name
      user.email = auth.info.email
      user.oauth_token = auth.credentials.token
      user.oauth_expires_at = Time.at(auth.credentials.expires_at)
      user.save!(validate: false)
    end
  end



end
