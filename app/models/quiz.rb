class Quiz < ActiveRecord::Base
	has_many :answers, dependent: :destroy
	has_many :campaigns

  belongs_to :admin

	accepts_nested_attributes_for :answers, reject_if: :all_blank, allow_destroy: true

	# def answers_for_form
	# 	collection = answers.where(answers_id: id)
	#     collection.any? ? collection : answers.build
	# end

end
