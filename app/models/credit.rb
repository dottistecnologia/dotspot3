class Credit < ActiveRecord::Base
  belongs_to :admin

  validates_presence_of :admin_id, :amount
  validates :amount, numericality: { greater_than: 0 }

  after_create :add_credit_to_client
  after_destroy :remove_credit_from_client

  private
    def add_credit_to_client
      self.admin.credit += self.amount
      self.admin.save
    end

    def remove_credit_from_client
      self.admin.credit -= self.amount
      self.admin.save
    end
end
