json.extract! technical_call, :id, :user_id, :description, :status, :created_at, :updated_at
json.url technical_call_url(technical_call, format: :json)