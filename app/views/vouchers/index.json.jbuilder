json.array!(@vouchers) do |voucher|
  json.extract! voucher, :id, :number, :time
  json.url voucher_url(voucher, format: :json)
end
