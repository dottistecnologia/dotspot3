json.extract! vote, :id, :mac, :quiz_id, :answer_id, :user_id, :created_at, :updated_at
json.url vote_url(vote, format: :json)