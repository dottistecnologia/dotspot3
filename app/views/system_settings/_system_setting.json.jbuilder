json.extract! system_setting, :id, :allow_guest_self_sign_up, :terms_and_conditions, :created_at, :updated_at
json.url system_setting_url(system_setting, format: :json)