json.extract! request, :id, :mac, :location, :ssid, :platform, :device, :browser, :created_at, :updated_at
json.url request_url(request, format: :json)