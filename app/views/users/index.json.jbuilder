json.array!(@users) do |user|
  json.extract! user, :id, :name, :gender, :category, :address, :number, :city, :state, :district, :code, :registry, :issuing, :cpf, :date, :validate, :registration, :company, :depend, :dependent
  json.url user_url(user, format: :json)
end
