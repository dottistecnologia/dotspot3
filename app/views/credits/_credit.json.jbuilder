json.extract! credit, :id, :admin_id, :amount, :created_at, :updated_at
json.url credit_url(credit, format: :json)