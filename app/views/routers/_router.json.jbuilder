json.extract! router, :id, :ip, :port, :location, :username, :password, :created_at, :updated_at
json.url router_url(router, format: :json)