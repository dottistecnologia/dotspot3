json.extract! answer, :id, :description, :quiz_id, :created_at, :updated_at
json.url answer_url(answer, format: :json)