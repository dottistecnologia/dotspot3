var Script = function () {

    $.validator.setDefaults({
        submitHandler: function() { alert("submitted!"); }
    });

    $().ready(function() {

        // if(document.getElementById('agree').checked) {
        //      $("#button").html('<button class="btn btn-lg btn-login btn-block" onclick="auth();" id="pulsate-regular"  type="button">CONECTAR</button>');
        // } else {
        //      $("#button").html('<button class="btn btn-lg btn-login btn-block" onclick="auth();" id="pulsate-regular"  type="button" >CONECTAR</button>');

        // }
        
        // validate signup form on keyup and submit
        $("#guest").validate({
            rules: {
               
                cpf: {
                    required: true,
                    minlength: 11,
                    number: true,
                    verificaCPF: true
                },
                fone: {
                    required: true,
                    number: true,
                    
                },
                email: {
                    required: true,
                    email: true
                    
                },
               
                date: {
                    required: true,
                    minlength: 8,
                    dateBR: true
                },
                agree: "required"
            },
            
            messages: {
                
                cpf: {
                    required: "Digite o CPF",
                    minlength: "O CPF deve conter 11 dígitos",
                    number: "Digite apenas número"
                },
                email: {
                    required: "Digite o Email",
                    minlength: "Email deve conter @",
                    number: "Digite apenas número"
                },

                date: {
                    required: "Digite a Data de Nascimento",
                    minlength: "A DATA DE NASCIMENTO deve conter no mínimo 10 dígitos",
                    // dateBR: "Por favor, entre com uma DATA válida. Ex: 10/06/1972"
                },
                
                agree: "Por favor, aceite os termos de utilização"
            }
            
        });

        jQuery.extend(jQuery.validator.messages, {
            required: "This field is required.",
            remote: "Please fix this field.",
            email: "Entre com um email válido.",
            url: "Please enter a valid URL.",
            date: "Please enter a valid date.",
            dateISO: "Please enter a valid date (ISO).",
            number: "Por favor digite somente números.",
            digits: "Please enter only digits.",
            creditcard: "Please enter a valid credit card number.",
            equalTo: "Please enter the same value again.",
            accept: "Please enter a value with a valid extension.",
            maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
            minlength: jQuery.validator.format("Please enter at least {0} characters."),
            rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
            range: jQuery.validator.format("Please enter a value between {0} and {1}."),
            max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
            min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
        });


        jQuery.validator.addMethod("verificaCPF", function(value, element) {
            value = value.replace('.','');
            value = value.replace('.','');
            cpf = value.replace('-','');
            while(cpf.length < 11) cpf = "0"+ cpf;
            var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
            var a = [];
            var b = new Number;
            var c = 11;
            for (i=0; i<11; i++){
                a[i] = cpf.charAt(i);
                if (i < 9) b += (a[i] * --c);
            }
            if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11-x }
            b = 0;
            c = 11;
            for (y=0; y<10; y++) b += (a[y] * c--);
            if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }
            if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)) return false;
            return true;
        }, "Informe um CPF válido.");

        jQuery.validator.addMethod("dateBR", function(value, element) {            
             //contando chars 
            if(value.length!=10) return false;
            // verificando data
            var data        = value;
            var dia         = data.substr(0,2);
            var barra1      = data.substr(2,1);
            var mes         = data.substr(3,2);         
            var barra2      = data.substr(5,1);
            var ano         = data.substr(6,4);         
            if(data.length!=10||barra1!="/"||barra2!="/"||isNaN(dia)||isNaN(mes)||isNaN(ano)||dia>31||mes>12)return false; 
            if((mes==4||mes==6||mes==9||mes==11)&&dia==31)return false;
            if(mes==2 && (dia>29||(dia==29&&ano%4!=0)))return false;
            if(ano < 1900)return false;
            return true;        
        }, "OPS! Parece que a data está errada. Exemplo: 12/04/1978");


     

      
    });


}();