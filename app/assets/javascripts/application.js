// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.

//  require turbolinks
//  require nprogress
//  require nprogress-turbolinks

//
//= require jquery
//= require jquery_ujs
//= require bootstrap.min
//= require jquery.cookie
//= require jquery.scrollTo.min
//= require jquery.nicescroll
// require jquery.sparkline
// require jquery.easy-pie-chart
//= require owl.carousel
//= require jquery.customSelect.min
//= require respond.min
//= require slidebars.min
// require sparkline-chart
// require easy-pie-chart
//= require respond.min
//= require count
//= require jquery.counterup.min
//= require jquery.validate
//= require jquery.validate.additional-methods
//= require jquery.dcjqaccordion.2.7.min
//= require common-scripts
//= require filterrific/filterrific-jquery
//= require mask
//= require CPF
//= require moment
//= require moment/pt-br.js
//= require bootstrap-datetimepicker
//= require cocoon
//= require toastr
//= require apexcharts


