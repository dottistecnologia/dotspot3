module ReportsHelper
  def to_csv_not_relation(options = {}, data, titles)
    CSV.generate(options) do |csv|
      csv << titles
      data.each do |obj|
        csv << obj
      end
    end
  end
end
