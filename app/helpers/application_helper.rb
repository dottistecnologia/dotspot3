module ApplicationHelper
    
    def set_route_form
        if current_page?(root_path)
            root_path
        elsif current_page?(requests_path)
            requests_path
        end
    end

end
