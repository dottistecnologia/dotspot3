module Guest::LoginHelper

  def check_if_is_working_time
    ss = SystemSetting.first
    is_working_time = false

    # Adiciona 1 dia pra fazer mais lógica com o dia da semana. Ex.: 2ª = 2.
    week_day = Time.now.wday + 1

    case week_day
    when 1
      if Time.zone.now.strftime("%H%M") >= ss.sunday_opens_at.strftime("%H%M") and Time.zone.now.strftime("%H%M") <= ss.sunday_closes_at.strftime("%H%M")
        is_working_time = true
      end
    when 2
      if Time.zone.now.strftime("%H%M") >= ss.monday_opens_at.strftime("%H%M") and Time.zone.now.strftime("%H%M") <= ss.monday_closes_at.strftime("%H%M")
        is_working_time = true
      end
    when 3
      if Time.zone.now.strftime("%H%M") >= ss.tuesday_opens_at.strftime("%H%M") and Time.zone.now.strftime("%H%M") <= ss.tuesday_closes_at.strftime("%H%M")
        is_working_time = true
      end
    when 4
      if Time.zone.now.strftime("%H%M") >= ss.wednesday_opens_at.strftime("%H%M") and Time.zone.now.strftime("%H%M") <= ss.wednesday_closes_at.strftime("%H%M")
        is_working_time = true
      end
    when 5
      if Time.zone.now.strftime("%H%M") >= ss.thursday_opens_at.strftime("%H%M") and Time.zone.now.strftime("%H%M") <= ss.thursday_closes_at.strftime("%H%M")
        is_working_time = true
      end
    when 6
      if Time.zone.now.strftime("%H%M") >= ss.friday_opens_at.strftime("%H%M") and Time.zone.now.strftime("%H%M") <= ss.friday_closes_at.strftime("%H%M")
        is_working_time = true
      end
    when 7
      if Time.zone.now.strftime("%H%M") >= ss.saturday_opens_at.strftime("%H%M") and Time.zone.now.strftime("%H%M") <= ss.saturday_closes_at.strftime("%H%M")
        is_working_time = true
      end
    end

    is_working_time
  end

end
