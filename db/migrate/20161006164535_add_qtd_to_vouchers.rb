class AddQtdToVouchers < ActiveRecord::Migration
  def change
    add_column :vouchers, :qtd, :string
    add_column :vouchers, :used, :boolean
  end
end
