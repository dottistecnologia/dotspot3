class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.string :description
      t.string :quiz_id

      t.timestamps null: false
    end
  end
end
