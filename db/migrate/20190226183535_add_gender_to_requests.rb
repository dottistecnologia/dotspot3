class AddGenderToRequests < ActiveRecord::Migration
  def change
    add_column :requests, :gender, :string
  end
end
