class AddWeightToCampaigns < ActiveRecord::Migration
  def change
    add_column :campaigns, :weight, :integer, default: 1
  end
end
