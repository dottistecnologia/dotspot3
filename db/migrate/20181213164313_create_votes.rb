class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.string :mac
      t.integer :quiz_id
      t.integer :answer_id
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
