class AddViewsToCampaigns < ActiveRecord::Migration
  def change
    add_column :campaigns, :views, :integer, default: 0
  end
end
