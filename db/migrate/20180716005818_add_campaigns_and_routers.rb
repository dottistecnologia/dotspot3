class AddCampaignsAndRouters < ActiveRecord::Migration
  def self.up
    create_table :campaigns_routers do |t|
      t.references :campaign, :router
    end
  end

  def self.down
    drop_table :campaigns_routers
  end
end
