class AddEmailToVouchers < ActiveRecord::Migration
  def change
    add_column :vouchers, :email, :string
  end
end
