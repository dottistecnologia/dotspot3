class AddColunsToSystemSettings < ActiveRecord::Migration
  def change
    add_column :system_settings, :allow_facebook_signup, :boolean, default: true
    add_column :system_settings, :facebook_checking, :boolean, default: true
    add_column :system_settings, :place_id_to_facebook_checkin, :string
  end
end
