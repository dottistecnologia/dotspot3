class CreateRequests < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.string :mac
      t.string :location
      t.string :ssid
      t.string :platform
      t.string :device
      t.string :browser

      t.timestamps null: false
    end
  end
end
