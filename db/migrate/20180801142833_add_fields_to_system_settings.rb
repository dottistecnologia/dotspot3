class AddFieldsToSystemSettings < ActiveRecord::Migration
  def change
    add_column :system_settings, :gender_visible, :boolean, default: false
    add_column :system_settings, :gender_required, :boolean, default: false
  end
end
