class AddAnotherFieldsToSystemSettings < ActiveRecord::Migration
  def change
    add_column :system_settings, :name_visible, :boolean
    add_column :system_settings, :name_required, :boolean
    add_column :system_settings, :address_visible, :boolean
    add_column :system_settings, :address_required, :boolean
    add_column :system_settings, :cpf_visible, :boolean
    add_column :system_settings, :cpf_required, :boolean
    add_column :system_settings, :birthday_visible, :boolean
    add_column :system_settings, :birthday_required, :boolean
    add_column :system_settings, :email_visible, :boolean
    add_column :system_settings, :email_required, :boolean
  end
end
