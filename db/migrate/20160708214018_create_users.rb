class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :gender
      t.string :category
      t.string :address
      t.string :number
      t.string :city
      t.string :state
      t.string :district
      t.string :code
      t.string :registry
      t.string :issuing
      t.string :cpf
      t.date :date
      t.string :registration
      t.string :company
      t.string :depend
      t.string :dependent

      t.timestamps null: false
    end
  end
end
