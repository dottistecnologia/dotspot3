class AddVideoToCampaigns < ActiveRecord::Migration
  def change
    add_column :campaigns, :video, :string
    add_column :campaigns, :default, :integer
  end
end
