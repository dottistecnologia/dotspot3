class CreateTechnicalCalls < ActiveRecord::Migration
  def change
    create_table :technical_calls do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.text :description
      t.integer :status

      t.timestamps null: false
    end
  end
end
