class CreateMacs < ActiveRecord::Migration
  def change
    create_table :macs do |t|
      t.string :mac
      t.belongs_to :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
