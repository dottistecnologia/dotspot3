class AddCampaignToRequests < ActiveRecord::Migration
  def change
    add_reference :requests, :campaign, index: true, foreign_key: true
  end
end
