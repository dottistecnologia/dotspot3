class AddImageFieldsToSystemSettings < ActiveRecord::Migration
  def change
    add_column :system_settings, :background, :string
    add_column :system_settings, :logo, :string
  end
end
