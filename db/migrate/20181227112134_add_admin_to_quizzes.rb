class AddAdminToQuizzes < ActiveRecord::Migration
  def change
    add_reference :quizzes, :admin, index: true, foreign_key: true
  end
end
