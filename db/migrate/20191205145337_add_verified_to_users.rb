class AddVerifiedToUsers < ActiveRecord::Migration
  def change
    add_column :users, :whatsapp_verified, :boolean, default: false
    add_column :users, :email_verified, :boolean, default: false
  end
end
