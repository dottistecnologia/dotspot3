class AddIpOutToRouters < ActiveRecord::Migration
  def change
    add_column :routers, :ip_out, :string
    add_column :routers, :port_out, :string
  end
end
