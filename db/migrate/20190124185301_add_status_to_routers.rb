class AddStatusToRouters < ActiveRecord::Migration
  def change
    add_column :routers, :status, :boolean
  end
end
