class CreateSystemSettings < ActiveRecord::Migration
  def change
    create_table :system_settings do |t|
      t.boolean :allow_guest_self_sign_up
      t.text :terms_and_conditions

      t.timestamps null: false
    end
  end
end
