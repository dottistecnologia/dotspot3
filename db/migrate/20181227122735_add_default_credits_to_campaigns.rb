class AddDefaultCreditsToCampaigns < ActiveRecord::Migration
  def up
    change_column :campaigns, :credits, :integer, default:
    Campaign.all.each { |c| c.update_attribute(:credits, 0) if c.credits == nil}
  end

  def down
    change_column :campaigns, :credits, :integer
  end
end
