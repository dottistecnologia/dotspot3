class AssignAlreadyCreatedCampaignsToAdmin < ActiveRecord::Migration
  def change
    Campaign.all.each {|c| c.update_column(:admin_id, Admin.administrador.first.id) if c.admin_id == nil}
  end
end
