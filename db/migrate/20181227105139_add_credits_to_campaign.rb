class AddCreditsToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :credits, :integer
  end
end
