class CreateRouters < ActiveRecord::Migration
  def change
    create_table :routers do |t|
      t.string :ip
      t.string :port
      t.string :location
      t.string :username
      t.string :password

      t.timestamps null: false
    end
  end
end
