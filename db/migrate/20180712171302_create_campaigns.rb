class CreateCampaigns < ActiveRecord::Migration
  def change
    create_table :campaigns do |t|
      t.string :name
      t.date :stard_date
      t.date :end_date
      t.string :imagem

      t.timestamps null: false
    end
  end
end
