class AddQuizIdToCampaigns < ActiveRecord::Migration
  def change
    add_column :campaigns, :quiz_id, :integer
  end
end
