class AddUnifiToSystemSettings < ActiveRecord::Migration
  def change
    add_column :system_settings, :host, :string
    add_column :system_settings, :port, :string
    add_column :system_settings, :site, :string
    add_column :system_settings, :user, :string
    add_column :system_settings, :password, :string
  end
end
