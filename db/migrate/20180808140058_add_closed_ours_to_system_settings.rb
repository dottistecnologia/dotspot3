class AddClosedOursToSystemSettings < ActiveRecord::Migration
  def change
    add_column :system_settings, :sunday_opens_at, :time
    add_column :system_settings, :sunday_closes_at, :time
    add_column :system_settings, :monday_opens_at, :time
    add_column :system_settings, :monday_closes_at, :time
    add_column :system_settings, :tuesday_opens_at, :time
    add_column :system_settings, :tuesday_closes_at, :time
    add_column :system_settings, :wednesday_opens_at, :time
    add_column :system_settings, :wednesday_closes_at, :time
    add_column :system_settings, :thursday_opens_at, :time
    add_column :system_settings, :thursday_closes_at, :time
    add_column :system_settings, :friday_opens_at, :time
    add_column :system_settings, :friday_closes_at, :time
    add_column :system_settings, :saturday_opens_at, :time
    add_column :system_settings, :saturday_closes_at, :time
  end
end
