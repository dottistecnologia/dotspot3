class AddTelephoneVisibleToSystemSetting < ActiveRecord::Migration
  def change
    add_column :system_settings, :telephone_visible, :boolean
    add_column :system_settings, :telephone_required, :boolean
  end
end
