class AddDeviceToUsers < ActiveRecord::Migration
  def change
    add_column :users, :mac, :string
    add_column :users, :ssid, :string
    add_column :users, :ap, :string
  end
end
