class AddPrintedToVouchers < ActiveRecord::Migration
  def change
    add_column :vouchers, :printed, :boolean
  end
end
