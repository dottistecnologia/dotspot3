class AddCreditToAdmins < ActiveRecord::Migration
  def change
    add_column :admins, :credit, :integer, default: 0
  end
end
