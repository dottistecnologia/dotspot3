class AddConnectionsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :connections, :integer, default: 0
  end
end
