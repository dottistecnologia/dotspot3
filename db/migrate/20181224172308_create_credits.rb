class CreateCredits < ActiveRecord::Migration
  def change
    create_table :credits do |t|
      t.belongs_to :admin, index: true, foreign_key: true
      t.integer :amount

      t.timestamps null: false
    end
  end
end
