class CreateVouchers < ActiveRecord::Migration
  def change
    create_table :vouchers do |t|
      t.string :number
      t.string :time

      t.timestamps null: false
    end
  end
end
