class AddFiedsToAdmins < ActiveRecord::Migration
  def up
    add_column :admins, :name, :string
    add_column :admins, :role, :integer
    add_column :admins, :cpf_cnpj, :string
    add_column :admins, :phone, :string

    Admin.all.each {|u| u.update_attribute(:role, 'administrador')}
  end

  def down
    remove_column :admins, :name
    remove_column :admins, :role
    remove_column :admins, :cpf_cnpj
    remove_column :admins, :phone
  end
end
