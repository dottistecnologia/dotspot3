# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20191219125051) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string   "email",              default: "",    null: false
    t.string   "encrypted_password", default: "",    null: false
    t.integer  "sign_in_count",      default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "failed_attempts",    default: 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "admin",              default: false
    t.string   "name"
    t.integer  "role"
    t.string   "cpf_cnpj"
    t.string   "phone"
    t.integer  "credit",             default: 0
  end

  create_table "answers", force: :cascade do |t|
    t.string   "description"
    t.string   "quiz_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "votes",       default: 0
  end

  create_table "campaigns", force: :cascade do |t|
    t.string   "name"
    t.date     "stard_date"
    t.date     "end_date"
    t.string   "imagem"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "views",      default: 0
    t.integer  "weight",     default: 1
    t.string   "video"
    t.integer  "default"
    t.integer  "quiz_id"
    t.integer  "credits"
    t.integer  "admin_id"
    t.boolean  "status"
  end

  add_index "campaigns", ["admin_id"], name: "index_campaigns_on_admin_id", using: :btree

  create_table "campaigns_routers", force: :cascade do |t|
    t.integer "campaign_id"
    t.integer "router_id"
  end

  create_table "credits", force: :cascade do |t|
    t.integer  "admin_id"
    t.integer  "amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "credits", ["admin_id"], name: "index_credits_on_admin_id", using: :btree

  create_table "macs", force: :cascade do |t|
    t.string   "mac"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "macs", ["user_id"], name: "index_macs_on_user_id", using: :btree

  create_table "quizzes", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "admin_id"
  end

  add_index "quizzes", ["admin_id"], name: "index_quizzes_on_admin_id", using: :btree

  create_table "requests", force: :cascade do |t|
    t.string   "mac"
    t.string   "location"
    t.string   "ssid"
    t.string   "platform"
    t.string   "device"
    t.string   "browser"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "campaign_id"
    t.integer  "user_id"
    t.string   "gender"
  end

  add_index "requests", ["campaign_id"], name: "index_requests_on_campaign_id", using: :btree

  create_table "routers", force: :cascade do |t|
    t.string   "ip"
    t.string   "port"
    t.string   "location"
    t.string   "username"
    t.string   "password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "ip_out"
    t.string   "port_out"
    t.boolean  "status"
  end

  create_table "system_settings", force: :cascade do |t|
    t.boolean  "allow_guest_self_sign_up"
    t.text     "terms_and_conditions"
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.boolean  "allow_facebook_signup",        default: true
    t.boolean  "facebook_checking",            default: true
    t.string   "place_id_to_facebook_checkin"
    t.boolean  "gender_visible",               default: false
    t.boolean  "gender_required",              default: false
    t.boolean  "name_visible"
    t.boolean  "name_required"
    t.boolean  "address_visible"
    t.boolean  "address_required"
    t.boolean  "cpf_visible"
    t.boolean  "cpf_required"
    t.boolean  "birthday_visible"
    t.boolean  "birthday_required"
    t.boolean  "email_visible"
    t.boolean  "email_required"
    t.string   "background"
    t.string   "logo"
    t.time     "sunday_opens_at"
    t.time     "sunday_closes_at"
    t.time     "monday_opens_at"
    t.time     "monday_closes_at"
    t.time     "tuesday_opens_at"
    t.time     "tuesday_closes_at"
    t.time     "wednesday_opens_at"
    t.time     "wednesday_closes_at"
    t.time     "thursday_opens_at"
    t.time     "thursday_closes_at"
    t.time     "friday_opens_at"
    t.time     "friday_closes_at"
    t.time     "saturday_opens_at"
    t.time     "saturday_closes_at"
    t.string   "host"
    t.string   "port"
    t.string   "site"
    t.string   "user"
    t.string   "password"
    t.boolean  "telephone_visible"
    t.boolean  "telephone_required"
  end

  create_table "technical_calls", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "description"
    t.integer  "status"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "technical_calls", ["user_id"], name: "index_technical_calls_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "gender"
    t.string   "category"
    t.string   "address"
    t.string   "number"
    t.string   "city"
    t.string   "state"
    t.string   "district"
    t.string   "code"
    t.string   "registry"
    t.string   "issuing"
    t.string   "cpf"
    t.date     "date"
    t.string   "registration"
    t.string   "company"
    t.string   "depend"
    t.string   "dependent"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "validity"
    t.string   "mac"
    t.string   "ssid"
    t.string   "ap"
    t.string   "package_id"
    t.boolean  "active"
    t.string   "email"
    t.string   "telephone"
    t.string   "voucher"
    t.string   "password"
    t.string   "profile"
    t.string   "provider"
    t.string   "uid"
    t.string   "oauth_token"
    t.datetime "oauth_expires_at"
    t.integer  "connections",       default: 0
    t.datetime "last_login"
    t.string   "location"
    t.boolean  "whatsapp_verified", default: false
    t.boolean  "email_verified",    default: false
  end

  create_table "votes", force: :cascade do |t|
    t.string   "mac"
    t.integer  "quiz_id"
    t.integer  "answer_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "vouchers", force: :cascade do |t|
    t.string   "number"
    t.string   "time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "qtd"
    t.boolean  "used"
    t.boolean  "printed"
    t.string   "email"
  end

  add_foreign_key "campaigns", "admins"
  add_foreign_key "credits", "admins"
  add_foreign_key "macs", "users"
  add_foreign_key "quizzes", "admins"
  add_foreign_key "requests", "campaigns"
  add_foreign_key "technical_calls", "users"
end
