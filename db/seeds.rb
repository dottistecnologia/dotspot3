# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Admin.destroy_all
Admin.create!(email: 'marllon@dottis.com.br', password: 'henmar22', admin: true)

SystemSetting.create!(
  allow_guest_self_sign_up: true,
  terms_and_conditions: '',
  gender_visible: false,
  gender_required: false,
  name_visible: true,
  name_required: true,
  cpf_visible: true,
  cpf_required: true,
  birthday_visible: true,
  birthday_required: true,
  email_visible: true,
  email_required: true,
  address_visible: false,
  address_required: false,
  place_id_to_facebook_checkin: "117146138441889",
  sunday_opens_at: '00:00',
  sunday_closes_at: '23:59',
  monday_opens_at: '00:00',
  monday_closes_at: '23:59',
  tuesday_opens_at: '00:00',
  tuesday_closes_at: '23:59',
  wednesday_opens_at: '00:00',
  wednesday_closes_at: '23:59',
  thursday_opens_at: '00:00',
  thursday_closes_at: '23:59',
  friday_opens_at: '00:00',
  friday_closes_at: '23:59',
  saturday_opens_at: '00:00',
  saturday_closes_at: '23:59'
  )
