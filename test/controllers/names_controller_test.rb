require 'test_helper'

class NamesControllerTest < ActionController::TestCase
  setup do
    @name = names(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:names)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create name" do
    assert_difference('Name.count') do
      post :create, name: { address: @name.address, category: @name.category, city: @name.city, code: @name.code, company: @name.company, cpf: @name.cpf, date: @name.date, depend: @name.depend, dependent: @name.dependent, district: @name.district, gender: @name.gender, issuing: @name.issuing, number: @name.number, registration: @name.registration, registry: @name.registry, state: @name.state, validate: @name.validate }
    end

    assert_redirected_to name_path(assigns(:name))
  end

  test "should show name" do
    get :show, id: @name
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @name
    assert_response :success
  end

  test "should update name" do
    patch :update, id: @name, name: { address: @name.address, category: @name.category, city: @name.city, code: @name.code, company: @name.company, cpf: @name.cpf, date: @name.date, depend: @name.depend, dependent: @name.dependent, district: @name.district, gender: @name.gender, issuing: @name.issuing, number: @name.number, registration: @name.registration, registry: @name.registry, state: @name.state, validate: @name.validate }
    assert_redirected_to name_path(assigns(:name))
  end

  test "should destroy name" do
    assert_difference('Name.count', -1) do
      delete :destroy, id: @name
    end

    assert_redirected_to names_path
  end
end
