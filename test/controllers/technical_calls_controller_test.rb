require 'test_helper'

class TechnicalCallsControllerTest < ActionController::TestCase
  setup do
    @technical_call = technical_calls(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:technical_calls)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create technical_call" do
    assert_difference('TechnicalCall.count') do
      post :create, technical_call: { description: @technical_call.description, status: @technical_call.status, user_id: @technical_call.user_id }
    end

    assert_redirected_to technical_call_path(assigns(:technical_call))
  end

  test "should show technical_call" do
    get :show, id: @technical_call
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @technical_call
    assert_response :success
  end

  test "should update technical_call" do
    patch :update, id: @technical_call, technical_call: { description: @technical_call.description, status: @technical_call.status, user_id: @technical_call.user_id }
    assert_redirected_to technical_call_path(assigns(:technical_call))
  end

  test "should destroy technical_call" do
    assert_difference('TechnicalCall.count', -1) do
      delete :destroy, id: @technical_call
    end

    assert_redirected_to technical_calls_path
  end
end
