namespace :db do
  desc "Popula tabela macs"
  task macs: :environment do
    puts 'Populando tabela Macs'
    @user = User.all
    @user.each do |user|
      unless Mac.find_by(mac: user.mac)
        Mac.create(user: user, mac: user.mac)
      end
    end
    puts 'Tabela macs populada com sucesso!'
  end

end
