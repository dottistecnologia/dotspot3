Rails.application.routes.draw do


  resources :credits
  resources :votes
  resources :answers
  resources :quizzes

  get 'reports/unique_guests', to: 'reports#unique_guests'
  get 'reports/times_with_more_hits', to: 'reports#times_with_more_hits'
  get 'reports/operating_sistems', to: 'reports#operating_sistems'
  get 'reports/devices', to: 'reports#devices'
  get 'reports/recurring_guests', to: 'reports#recurring_guests'
  get 'reports/registered_users', to: 'reports#registered_users'

  resources :requests
  resources :technical_calls do
    collection do
      get 'open'
    end
  end
  get 'auth/:provider/callback', to: 'guest/login#login_facebook', via: [:get, :post]
  get 'auth/failure', to: redirect('/'), via: [:get, :post]
  # match 'signout', to: 'sessions#destroy', as: 'signout'
  get 'signout', to: redirect('/'), via: [:get, :post]

  resources :campaigns do
    collection do
      get 'view'
      get 'votes'
      get 'disable'
      get 'enable'
      post 'disable'
    end
  end

  resources :system_settings, only: [:update] do
    collection do
      get 'my'
    end
  end



  resources :routers
  resources :vouchers do
    collection do
      get 'list'
      delete 'destroy_multiple'
      get 'print'
    end
  end

  get 'users_per_day' => 'charts#users_per_day'
  get 'last_campaign' => 'charts#last_campaign'
  get 'vouchers_use' => 'charts#vouchers_use'

  resources :users do
    collection do
      post 'destroy_multiple'
      get 'active'
      get 'listmac'
      get 'listprofile'
      get 'removemac'
      get 'removeprofile'
      get 'remove'
      get 'verify_whatsapp'
      get 'verify_email'
      post 'addmac'
      post 'addprofile'
      get 'guest_cadastro'
      post 'create_guest'
      get 'forgot_password'
      post 'change_password'
      get 'export'
      post 'api_voucher'
    end
  end

  resources :admins do
    collection do
      get 'clients'
      get 'new_client'
      delete 'destroy_multiple'
    end
    member do
      get 'credit_history'
    end
  end

  # devise_for :admins

  devise_for :admins, path: "auth", path_names: { sign_in: 'login', sign_out: 'logout', password: 'secret', confirmation: 'verification', unlock: 'unblock', registration: 'register', sign_up: 'cmon_let_me_in' }

  get 'home/index'
  get 'home/clients'
  get 'home/list_aps'
  post 'home/list_aps'
  get 'home/top_users'

  namespace :guest do

    post 'auth' => 'login#auth'
    post 'comerciarios' => 'login#comerciarios'
    post 'hospedes' => 'login#hospedes'
    post 'geral' => 'login#geral'
    post 'giraffas' => 'login#giraffas'

    post 'caldas_cpf' => 'login#caldas_cpf'
    post 'create_api_caldas' => 'login#create_api_caldas'
    post 'create_api_atalaia' => 'login#create_api_atalaia'
    post 'api_convidados' => 'login#api_convidados'
    post 'api_voucher' => 'login#api_voucher'

    post 'cadgiraffas' => 'login#cadgiraffas'
    post 'riovermelhocorp' => 'login#riovermelhocorp'


    get 'corporativo' => 'login#indexcorp'
    get 'cpf' => 'login#cpf'
    get 'caldas' => 'login#caldas'

    get 'novo' => 'login#novo'
    get 'pspr' => 'login#pspr'
    get 'novo_login_facebook' => 'login#novo_login_facebook'
    post 'novo_login' => 'login#novo_login'
    post 'novo_login_pspr' => 'login#novo_login_pspr'

    root 'login#novo'
    # root 'login#index'
  end

  namespace :api do
    namespace :v1 do
      get 'campagins' => 'auth#campaigns'
      get 'quiz' => 'auth#quiz'
      get 'answers' => 'auth#answers'
      get 'votes' => 'auth#votes'
      get 'connect' => 'auth#connect'
    end
  end


  root :to => "home#index"
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
